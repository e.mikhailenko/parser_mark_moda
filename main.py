import random
from pprint import pprint
import uuid

import pandas as pd
from googletrans import Translator
import urllib
from urllib.request import ProxyHandler, build_opener
import requests
from bs4 import BeautifulSoup
import pyautogui
import json
import os

women_clothes_link = 'https://mark.moda/catalog/2/products?utf8=%E2%9C%93&per_page=1162&by=tile'
women_shoes_link = 'https://mark.moda/catalog/86/products?utf8=%E2%9C%93&per_page=1162&by=tile'
women_bags_link = 'https://mark.moda/catalog/119/products?utf8=%E2%9C%93&per_page=1162&by=tile'
women_accessories_link = 'https://mark.moda/catalog/131/products?utf8=%E2%9C%93&per_page=1162&by=tile'
women_decorations_link = 'https://mark.moda/catalog/154/products?utf8=%E2%9C%93&per_page=1162&by=tile'
men_clothes_link = 'https://mark.moda/catalog/171/products?per_page=1162'
men_shoes_link = 'https://mark.moda/catalog/234/products?per_page=1162'
men_bags_link = 'https://mark.moda/catalog/262/products?per_page=1162'
men_accessories_link = 'https://mark.moda/catalog/272/products?per_page=1162'
men_decorations_link = 'https://mark.moda/catalog/294/products?per_page=1162'
parsed_file_name = 'files/men_clothes_parsed.xlsx'
product_detail_info_file = 'dressing_room_clothes_mapping_files/all_clothes_detail_info_full.xlsx'
all_models_file = 'files/all_models_table.xlsx'
json_file = 'dressing_room_clothes_mapping_files/all_clothes.json'
csv_file = 'files/all_models.csv'
resource_json_file = 'files/resources_fixed_legs_photo.json'
all_clothes_file = 'dressing_room_clothes_mapping_files/all_clothes_links.xlsx'
all_clothes_detail_info_file = 'dressing_room_clothes_mapping_files/full_parsing_clothes_5.xlsx'

ids_list = []
_ids_list = []
product_num_list = []
href_list = []
name_list = []
brand_list = []
categories_list = []
category_single_list = []
description_list = []
article_list = []
structure_list = []
collection_list = []
country_list = []
color_list = []
images_list = []
main_images_list = []
photo_list = []
back_photo_list = []
gender_list = []

param_lists_dict = {
    'Артикул': article_list,
    'Состав': structure_list,
    'Коллекция': collection_list,
    'Страна производства': country_list,
    'Цвет': color_list
}

name_model_list = []
instagram_list = []
type_body_list = []
type_body_id_list = []
char_images_id_list = []
gender_list = []
path_image_body_list = []
path_legs_photo_list = []
path_back_hair_photo_list = []
path_head_photo_list = []
path_title_image_list = []
type_body_id_list = []
char_images_id_list = []


def get_all_clothes():
    response = requests.get(men_clothes_link)
    html = response.text
    soup = BeautifulSoup(html, 'html.parser')
    products = soup.findAll('div', {'class': 'col-6 col-lg-4 mb-4'})
    for count, product in enumerate(products, 1):
        href = product.find('a').get('href')
        href_list.append(href)
        brand = product.find('p', {'class': 'brand'}).text
        brand_list.append(brand)
        name = product.find('div', {'class': 'name'}).text
        name_list.append(name)
        print(count)
    df = pd.DataFrame({
        'name': name_list,
        'brand': brand_list,
        'href': href_list
    })
    writer = pd.ExcelWriter(parsed_file_name)
    df.to_excel(writer, 'Sheet1', index=False)
    writer.save()
    print('done')


def get_li_rows(li_list):
    text = ''
    for li in li_list:
        text += li.text + ' > '
    return text


def get_detail_product_info():
    df = pd.read_excel('mapping_extra/product_ids_hm_bra_panties.xlsx')
    is_blocked_list = []
    hrefs = df['product_id']
    for count, href in enumerate(hrefs, 1):
        # if count > 3:
        #     break
        print(count)
        try:
            # product_num = href.split('/')[2]
            product_link = f'https://mark.moda/products/{href}'
            print(product_link)
            try:
                response = requests.get(product_link)
                html = response.text
                soup = BeautifulSoup(html, 'html.parser')
            except:
                continue
            brand = soup.find('h3', {'class': 'no-margin-top'}).find('a').text
            name = soup.find('h1', {'class': 'name'}).text
            description = soup.find('p', {'class': 'mb-5'}).text.replace('\n', '')
            breadcrumb = soup.find('ol', {'class': 'breadcrumb'})
            category = get_li_rows(breadcrumb.findAll('a'))

            product_num_list.append(href)
            name_list.append(name)
            brand_list.append(brand)
            description_list.append(description)
            categories_list.append(category)
            is_blocked_list.append(False)

            description_table = soup \
                .find('table', {'class': 'table table-border-none product-description-table'}) \
                .findAll('tr')
            for descr_item in description_table:
                item_name = descr_item.find('span', {'class': 'text-nowrap'}).text
                item_value = descr_item.find('td').text
                if item_name in param_lists_dict:
                    param_lists_dict[item_name].append(item_value)
                else:
                    print(item_name)
            main_image_src = soup.find('div', {'id': 'product-image'}).find('img').get('src')
            main_image = main_image_src.split('/')[7].split('.')[0].split('_')[0]
            main_images_list.append(main_image)
            images_list.append(f'{href}.jpg')
            urllib.request.urlretrieve(f'https://mark.moda{main_image_src}',
                                       f'all_images_women_hm_bra_panties/{href}.jpg')

        except:
            continue

    pprint({
        'product_num': len(product_num_list),
        'name': len(name_list),
        'brand': len(brand_list),
        'is_blocked': len(is_blocked_list),
        'description': len(description_list),
        'category': len(categories_list),
        'article': len(article_list),
        'structure': len(structure_list),
        'collection': len(collection_list),
        'country': len(country_list),
        'color': len(color_list),
        'image': len(images_list),
        'main_image_id': len(main_images_list)
    })
    df = pd.DataFrame({
        'product_num': product_num_list,
        'name': name_list,
        'brand': brand_list,
        'description': description_list,
        'is_blocked': is_blocked_list,
        'category': categories_list,
        'article': article_list,
        'structure': structure_list,
        'collection': collection_list,
        'country': country_list,
        'color': color_list,
        'image': images_list,
        'main_image_id': main_images_list
    })
    writer = pd.ExcelWriter('mapping_extra/parsed_clothes_women_hm_bra_panties.xlsx')
    df.to_excel(writer, 'Sheet1', index=False)
    writer.save()
    print('done')


def toggle_product():
    link = 'https://mark.moda/characters/new?toggle_product_id=5264'
    response = requests.get(link)
    html = response.text
    soup = BeautifulSoup(html, 'html.parser')
    pprint(soup)


def autopilot_mouse():
    count = 0
    seconds = 0.5
    while count < 200:
        pyautogui.moveTo(520, 520, seconds)
        pyautogui.leftClick()
        pyautogui.moveTo(650, 520, seconds)
        pyautogui.leftClick()
        pyautogui.moveTo(780, 520, seconds)
        pyautogui.leftClick()
        pyautogui.moveTo(900, 520, seconds)
        pyautogui.leftClick()
        if count % 7 == 0:
            pyautogui.scroll(-5)
        else:
            pyautogui.scroll(-4.8)
        count += 1
        pyautogui.sleep(1)


def get_resources():
    json_file_1 = 'files/resources.json'
    df = pd.read_json(json_file_1)
    empty = None
    for index, row in df.iterrows():
        list_back_hair = []
        list_head_photo = []
        list_title_images = []
        if row['gender'] == 'male':
            body_images_dir = 'images_men_models/image/photo'
            legs_photo_dir = 'images_men_models/image/legs_photo'
            back_hair_photo_dir = 'images_men_models/mannequin_head/back_hair_photo'
            head_photo_dir = 'images_men_models/mannequin_head/photo'
            title_image_dir = 'images_men_models/mannequin_head/title_image'
        elif row['gender'] == 'female':
            body_images_dir = 'images_women_models/image/photo'
            legs_photo_dir = 'images_women_models/image/legs_heels_photo'
            back_hair_photo_dir = 'images_women_models/mannequin_head/back_hair_photo'
            head_photo_dir = 'images_women_models/mannequin_head/photo'
            title_image_dir = 'images_women_models/mannequin_head/title_image'
        path_body_images = f'{body_images_dir}/{row["type_body_id"]}'
        try:
            files_image_body = os.listdir(path_body_images)
            path_image_body = f'{path_body_images}/{files_image_body[0]}'
        except:
            path_image_body = empty
        path_image_body_list.append(path_image_body)

        path_legs_photos = f'{legs_photo_dir}/{row["type_body_id"]}'
        try:
            files_legs_photo = os.listdir(path_legs_photos)
            path_legs_photo = f'{path_legs_photos}/{files_legs_photo[0]}'
        except:
            path_legs_photo = empty
        path_legs_photo_list.append(path_legs_photo)
        for count, image_id in enumerate(row['char_images_id']):
            path_back_hair_photo_dir = f'{back_hair_photo_dir}/{image_id}'
            try:
                files_back_hair_photo = os.listdir(path_back_hair_photo_dir)
                path_back_hair_photo = f'{path_back_hair_photo_dir}/{files_back_hair_photo[0]}'
            except:
                path_back_hair_photo = None
            print(path_back_hair_photo)
            list_back_hair.append(path_back_hair_photo)

            path_head_photo_dir = f'{head_photo_dir}/{image_id}'
            try:
                files_head_photo = os.listdir(path_head_photo_dir)
                path_head_photo = f'{path_head_photo_dir}/{files_head_photo[0]}'
            except:
                path_head_photo = None
            list_head_photo.append(path_head_photo)

            path_title_image_dir = f'{title_image_dir}/{image_id}'
            try:
                files_title_image = os.listdir(path_title_image_dir)
                path_title_image = f'{path_title_image_dir}/{files_title_image[0]}'
            except:
                path_title_image = None
            list_title_images.append(path_title_image)
        path_back_hair_photo_list.append(list_back_hair)
        path_head_photo_list.append(list_head_photo)
        path_title_image_list.append(list_title_images)
        name_model_list.append(row['name_model'])
        instagram_list.append(row['instagram'])
        type_body_list.append(row['type_body'])
        gender_list.append(row['gender'])
        type_body_id_list.append(row['type_body_id'])
        char_images_id_list.append(row['char_images_id'])

    df_resources = pd.DataFrame({
        'name_model': name_model_list,
        'instagram': instagram_list,
        'type_body': type_body_list,
        'gender': gender_list,
        'type_body_id': type_body_id_list,
        'char_images_id': char_images_id_list,
        'path_image_body': path_image_body_list,
        'path_legs_photo': path_legs_photo_list,
        'paths_back_hair_photo': path_back_hair_photo_list,
        'paths_head_photo': path_head_photo_list,
        'paths_title_image': path_title_image_list,
    })
    result = df_resources.to_json(orient='table', index=False)
    parsed = json.loads(result)
    with open(resource_json_file, 'w', encoding='utf-8') as f:
        json.dump(parsed, f, ensure_ascii=False, indent=4)


def get_links_for_dressing_room_clothes():
    image_names_list = []
    image_paths_list = []
    types_list = []
    images_ids_list = []
    dressing_room_images_dir = 'images_extra'
    all_dressing_room_dirs = os.listdir(dressing_room_images_dir)
    all_dressing_room_dirs.sort()
    for dressing_room_dir in all_dressing_room_dirs:
        photo_dir = f'{dressing_room_images_dir}/{dressing_room_dir}/photo'
        back_photo_dir = f'{dressing_room_images_dir}/{dressing_room_dir}/back_photo'
        all_photos = os.listdir(photo_dir)
        all_photos.sort()
        try:
            all_back_photos = os.listdir(back_photo_dir)
            all_back_photos.sort()
            for back_photo in all_back_photos:
                image_back_photo_dir = f'{back_photo_dir}/{back_photo}'
                image_file = os.listdir(image_back_photo_dir)[0]
                if 'dressing_room' in image_file:
                    image_names_list.append(image_file)
                    full_path_back_photo = f'{image_back_photo_dir}/{image_file}'.replace(
                        'images_extra/',
                        '')
                    image_paths_list.append(full_path_back_photo)
                    types_list.append('back_photo')
                    image_id = image_file.split('@')[0].split('_')[2].replace('+b', '').replace('+', '')
                    print(image_id)
                    images_ids_list.append(image_id)
        except:
            pass
        for photo in all_photos:
            image_photo_dir = f'{photo_dir}/{photo}'
            image_file = os.listdir(image_photo_dir)[0]
            if 'dressing_room' in image_file:
                image_names_list.append(image_file)
                full_path_photo = f'{image_photo_dir}/{image_file}'.replace('images_extra/', '')
                image_paths_list.append(full_path_photo)
                types_list.append('photo')
                image_id = image_file.split('@')[0].split('_')[2].replace('+b', '')
                print(image_id)
                images_ids_list.append(image_id)

    df = pd.DataFrame({
        'image_id': images_ids_list,
        'image_name': image_names_list,
        'type': types_list,
        'image_path': image_paths_list
    })

    writer = pd.ExcelWriter('mapping_extra/dressing_room_images_mapping_women_hm_bra_panties.xlsx')
    df.to_excel(writer, 'Sheet1', index=False)
    writer.save()
    print('done')


def dressing_room_photos_mapping():
    is_blocked_list = []
    df_images_paths = pd.read_excel('mapping_extra/dressing_room_images_mapping_women_hm_bra_panties.xlsx')
    df_clothes_detail_info = pd.read_excel('mapping_extra/parsed_clothes_women_hm_bra_panties.xlsx')
    df_clothes_detail_info = df_clothes_detail_info.drop_duplicates()
    for index, row in df_clothes_detail_info.iterrows():
        # if index > 1000:
        #     break
        product_num_list.append(row['product_num'])
        name_list.append(row['name'])
        brand_list.append(row['brand'])
        description_list.append(row['description'])
        categories_list.append(row['category'])
        article_list.append(row['article'])
        structure_list.append(row['structure'])
        collection_list.append(row['collection'])
        country_list.append(row['country'])
        color_list.append(row['color'])
        images_list.append(row['image'])
        is_blocked_list.append(False)

        # main_image_id = str(row['main_image_id'])
        # photo_path = df_images_paths.loc[df_images_paths['image_id'] == row['main_image_id']]['image_path']
        photo_path = \
            df_images_paths.loc[
                ((df_images_paths['type'] == 'photo') & (df_images_paths['image_id'] == row['main_image_id']))][
                'image_path']
        print(photo_path)
        if not photo_path.empty:
            photo_list.append(photo_path.iloc[0])
        else:
            photo_list.append(None)

        back_photo_path = \
            df_images_paths.loc[
                ((df_images_paths['type'] == 'back_photo') & (df_images_paths['image_id'] == row['main_image_id']))][
                'image_path']
        if not back_photo_path.empty:
            back_photo_list.append(back_photo_path.iloc[0])
        else:
            back_photo_list.append(None)

    df = pd.DataFrame({
        'product_num': product_num_list,
        'name': name_list,
        'brand': brand_list,
        'is_blocked': is_blocked_list,
        'description': description_list,
        'category': categories_list,
        'article': article_list,
        'structure': structure_list,
        'collection': collection_list,
        'country': country_list,
        'color': color_list,
        'image': images_list,
        'photo_path': photo_list,
        'back_photo_path': back_photo_list
    })
    all_clothes_detail_info_full = 'mapping_extra/clothes_women_hm_bra_panties_with_photos_and_back_photos.xlsx'
    writer = pd.ExcelWriter(all_clothes_detail_info_full)
    df.to_excel(writer, 'Sheet1', index=False)
    writer.save()
    print('done')


def is_blocked_mapping():
    is_blocked_list = []
    df_clothes = pd.read_json('mapping_extra/clothes.json')
    df_is_blocked_info = pd.read_excel('mapping_extra/product_ids_blocked.xlsx')
    df_is_blocked_info = df_is_blocked_info.drop_duplicates()
    for index, row in df_clothes.iterrows():
        print(index)
        ids_list.append(row['_id'])
        product_num_list.append(row['product_num'])
        name_list.append(row['name'])
        brand_list.append(row['brand'])
        description_list.append(row['description'])
        article_list.append(row['article'])
        structure_list.append(row['structure'])
        collection_list.append(row['collection'])
        country_list.append(row['country'])
        color_list.append(row['color'])
        images_list.append(row['image'])
        photo_list.append(row['front_photo_path'])
        back_photo_list.append(row['back_photo_path'])
        gender_list.append(row['sex'])
        categories_list.append(row['categories'])
        category_single_list.append(row['category'])
        is_blocked = df_is_blocked_info.loc[
            (df_is_blocked_info['product_id'] == row['product_num'])]['is_blocked']
        if not is_blocked.empty:
            is_blocked_list.append(is_blocked.iloc[0])
        else:
            is_blocked_list.append(True)
        # print(is_blocked)

    df = pd.DataFrame({
        '_id': ids_list,
        'product_num': product_num_list,
        'name': name_list,
        'sex': gender_list,
        'brand': brand_list,
        'is_blocked': is_blocked_list,
        'description': description_list,
        'categories': categories_list,
        'category': category_single_list,
        'article': article_list,
        'structure': structure_list,
        'collection': collection_list,
        'country': country_list,
        'color': color_list,
        'image': images_list,
        'front_photo_path': photo_list,
        'back_photo_path': back_photo_list
    })

    json_file_1 = 'mapping_extra/all_clothes_add_is_blocked.json'
    result = df.to_json(orient='table', index=False)
    parsed = json.loads(result)
    with open(json_file_1, 'w', encoding='utf-8') as f:
        json.dump(parsed, f, ensure_ascii=False, indent=4)


def parsing_static_html():
    product_id_list = []
    is_blocked_list = []
    html_pages = os.listdir('html_pages_extra')
    html_pages.sort()
    for html_page in html_pages:
        html_file = open(f'html_pages_extra/{html_page}').read()
        soup = BeautifulSoup(html_file, 'html.parser')
        products = soup.findAll('div', {'class': 'col-sm-3 dressing-room-tile-wrap'})
        for count, product in enumerate(products):
            print(count)
            is_blocked = product.find('div', {'class': 'dressing-room-tile-box-disabled'})
            if is_blocked is None:
                is_blocked_list.append('false')
            else:
                is_blocked_list.append('true')
            product_id = product.get('id').split('_')[1]
            # product_id_list.append(f'/products/{product_id}')
            product_id_list.append(product_id)

    df = pd.DataFrame({
        'product_id': product_id_list,
        'is_blocked': is_blocked_list,
    })

    writer = pd.ExcelWriter('mapping_extra/product_ids_women_skirts.xlsx')
    df.to_excel(writer, 'Sheet1', index=False)
    writer.save()
    print('done')


def fix_empty_into_all_clothes():
    df = pd.read_excel('mapping_extra/all_clothes_with_photos_and_back_photos.xlsx')
    df = df[df['photo_path'] is not None].drop_duplicates()
    df['image'] = df['product_num'].apply(lambda x: f'all_images/{str(x)}.jpg')

    writer = pd.ExcelWriter('mapping_extra/all_clothes_with_photos_and_back_photos.xlsx')
    df.to_excel(writer, 'Sheet1', index=False)
    writer.save()
    print('done')


def fix_exel_to_json():
    df = pd.read_excel('mapping_extra/clothes_women_hm_bra_panties_with_photos_and_back_photos.xlsx')
    json_file_1 = 'mapping_extra/clothes_women_hm_bra_panties_with_photos_and_back_photos.json'
    # df['char_images_id'] = df['char_images_id'].apply(
    #     lambda x: x.replace('[', '').replace(']', '').replace(',', '').split())
    # df['char_images_id'] = df['char_images_id'].apply(lambda x: list(map(int, x)))
    result = df.to_json(orient='table', index=False)
    parsed = json.loads(result)
    with open(json_file_1, 'w', encoding='utf-8') as f:
        json.dump(parsed, f, ensure_ascii=False, indent=4)


def fix_json_all_clothes():
    df_json = pd.read_json('mapping_extra/clothes_women_hm_bra_panties_with_photos_and_back_photos.json')
    print(df_json)
    for index, row in df_json.iterrows():

        ids_list.append(str(uuid.uuid4()).replace('-', ''))
        product_num_list.append(row['product_num'])
        name_list.append(row['name'])
        brand_list.append(row['brand'])
        description_list.append(row['description'])
        article_list.append(row['article'])
        structure_list.append(row['structure'])
        collection_list.append(row['collection'])
        country_list.append(row['country'])
        color_list.append(row['color'])
        images_list.append(row['image'])
        photo_list.append(row['photo_path'])
        if row['back_photo_path'] == 'empty':
            back_photo_list.append(None)
        else:
            back_photo_list.append(row['back_photo_path'])

        if row['category'].find('Для него') != -1:
            gender_list.append('male')
        else:
            gender_list.append('female')

        categories_fixed = row['category'].split(' > ')[2:]
        categories_fixed.pop(-1)
        categories_list.append(categories_fixed)
        category_single_list.append(categories_fixed[-1])

    df = pd.DataFrame({
        '_id': ids_list,
        'product_num': product_num_list,
        'name': name_list,
        'sex': gender_list,
        'brand': brand_list,
        'description': description_list,
        'categories': categories_list,
        'category': category_single_list,
        'article': article_list,
        'structure': structure_list,
        'collection': collection_list,
        'country': country_list,
        'color': color_list,
        'image': images_list,
        'front_photo_path': photo_list,
        'back_photo_path': back_photo_list
    })

    json_file_1 = 'mapping_extra/clothes_women_hm_bra_panties_with_photos_and_back_photos.json'
    result = df.to_json(orient='table', index=False)
    parsed = json.loads(result)
    with open(json_file_1, 'w', encoding='utf-8') as f:
        json.dump(parsed, f, ensure_ascii=False, indent=4)


def fix_resources():
    df_json = pd.read_json('files/resources_fixed_legs_photo.json')
    for index, row in df_json.iterrows():
        ids_list.append(str(uuid.uuid4()))
        path_image_body_list.append(row['path_image_body'])
        path_legs_photo_list.append(row['path_legs_photo'])
        path_head_photo_list.append(row['paths_head_photo'])
        path_back_hair_photo_list.append(row['paths_back_hair_photo'])
        path_title_image_list.append(row['paths_title_image'])
        name_model_list.append(row['name_model'])
        instagram_list.append(row['instagram'])
        type_body_list.append(row['type_body'])
        gender_list.append(row['gender'])

    df_resources = pd.DataFrame({
        'id': ids_list,
        'name_model': name_model_list,
        'instagram': instagram_list,
        'type_body': type_body_list,
        'sex': gender_list,
        'path_image_body': path_image_body_list,
        'path_legs_photo': path_legs_photo_list,
        'paths_back_hair_photo': path_back_hair_photo_list,
        'paths_head_photo': path_head_photo_list,
        'paths_title_image': path_title_image_list,
    })

    json_file_1 = 'dressing_room_clothes_mapping_files/resources_fixed_legs_photo.json'
    result = df_resources.to_json(orient='table', index=False)
    parsed = json.loads(result)
    with open(json_file_1, 'w', encoding='utf-8') as f:
        json.dump(parsed, f, ensure_ascii=False, indent=4)


def avatars_translate():
    df_json = pd.read_json('json_translate/avatars.json')
    translator = Translator()
    for index, row in df_json.iterrows():
        print(index)
        ids_list.append(row['id'])
        _ids_list.append(row['_id'])
        path_image_body_list.append(row['path_image_body'])
        path_legs_photo_list.append(row['path_legs_photo'])
        path_head_photo_list.append(row['paths_head_photo'])
        path_back_hair_photo_list.append(row['paths_back_hair_photo'])
        path_title_image_list.append(row['paths_title_image'])
        try:
            translate_name_model = translator.translate(row['name_model'], dest='en')
            print(translate_name_model.text)
            name_model_list.append(translate_name_model.text)
        except:
            name_model_list.append(row['name_model'])
        instagram_list.append(row['instagram'])
        # print(row['type_body'])
        try:
            if row['type_body'] == 'Негроидный':
                type_body_list.append('Negroid')
            elif row['type_body'] == 'Андрогины':
                type_body_list.append('Androgens')
            elif row['type_body'] == 'Негроидный/Латинский':
                type_body_list.append('Negroid/Latin')
            else:
                translate_type_body = translator.translate(row['type_body'], src='ru', dest='en')
                type_body_list.append(translate_type_body.text)
        except:
            type_body_list.append(row['type_body'])

        gender_list.append(row['sex'])

    df = pd.DataFrame({
        'id': ids_list,
        'name_model': name_model_list,
        'instagram': instagram_list,
        'type_body': type_body_list,
        'sex': gender_list,
        'path_image_body': path_image_body_list,
        'path_legs_photo': path_legs_photo_list,
        'paths_back_hair_photo': path_back_hair_photo_list,
        'paths_head_photo': path_head_photo_list,
        'paths_title_image': path_title_image_list,
        '_id': _ids_list,
    })

    json_file_1 = 'json_translate/avatars_translated.json'
    result = df.to_json(orient='table', index=False)
    parsed = json.loads(result)
    with open(json_file_1, 'w', encoding='utf-8') as f:
        json.dump(parsed, f, ensure_ascii=False, indent=4)
    print('done')


def layers_translate():
    translator = Translator()
    with open('json_translate/layers.json', 'r', encoding='utf-8') as f:
        text = json.load(f)

    layers_translated = {}
    for index, key in enumerate(text):
        print(index)
        try:
            key_translated = translator.translate(key, src='ru', dest='en')
            layers_translated[key_translated.text] = text[key]
        except:
            layers_translated[key] = text[key]

    with open('json_translate/layers_translated.json', 'w', encoding='utf-8') as f:
        json.dump(layers_translated, f, ensure_ascii=False, indent=4)
    print('done')


def clothes_translate():
    is_blocked_list = []
    df_json = pd.read_json('mapping_extra/clothes_women_hm_bra_panties_with_photos_and_back_photos.json')
    print(df_json)
    translator = Translator()
    for index, row in df_json.iterrows():
        print(index)
        # if index > 3:
        #     break
        # ids_list.append(row['id'])
        _ids_list.append(row['_id'])
        product_num_list.append(row['product_num'])
        try:
            translated_name = translator.translate(row['name'], src='ru', dest='en')
            name_list.append(translated_name.text)
        except:
            name_list.append(row['name'])
        brand_list.append(row['brand'])
        # try:
        #     translated_descr = translator.translate(row['description'], src='ru', dest='en')
        #     description_list.append(translated_descr.text)
        # except:
        description_list.append(row['description'])
        article_list.append(row['article'])

        # try:
        #     structure_translated = translator.translate(row['structure'], src='ru', dest='en')
        #     structure_list.append(structure_translated.text)
        # except:
        structure_list.append(row['structure'])

        # try:
        #     collection_translated = translator.translate(row['collection'], src='ru', dest='en')
        #     collection_list.append(collection_translated.text)
        # except:
        collection_list.append(row['collection'])

        try:
            country_translated = translator.translate(row['country'], src='ru', dest='en')
            country_list.append(country_translated.text)
        except:
            country_list.append(row['country'])

        try:
            color_translated = translator.translate(row['color'], src='ru', dest='en')
            color_list.append(color_translated.text)
        except:
            color_list.append(row['color'])

        try:
            category_translated = translator.translate(row['category'], src='ru', dest='en')
            category_single_list.append(category_translated.text)
        except:
            category_single_list.append(row['category'])

        # try:
        #     categories_translated = translator.translate(row['categories'], src='ru', dest='en')
        #     cat_trans_list = []
        #     for cat_trans in categories_translated:
        #         cat_trans_list.append(cat_trans.text)
        #     categories_list.append(cat_trans_list)
        # except:
        categories_list.append(row['categories'])

        images_list.append(row['image'])
        photo_list.append(row['front_photo_path'])
        back_photo_list.append(row['back_photo_path'])
        gender_list.append(row['sex'])
        is_blocked_list.append(False)

    df = pd.DataFrame({
        '_id': _ids_list,
        'product_num': product_num_list,
        'name': name_list,
        'sex': gender_list,
        'brand': brand_list,
        'is_blocked': is_blocked_list,
        'description': description_list,
        'categories': categories_list,
        'category': category_single_list,
        'article': article_list,
        'structure': structure_list,
        'collection': collection_list,
        'country': country_list,
        'color': color_list,
        'image': images_list,
        'front_photo_path': photo_list,
        'back_photo_path': back_photo_list
    })

    json_file_1 = 'mapping_extra/clothes_women_hm_bra_panties_translated.json'
    result = df.to_json(orient='table', index=False)
    parsed = json.loads(result)
    with open(json_file_1, 'w', encoding='utf-8') as f:
        json.dump(parsed, f, ensure_ascii=False, indent=4)


def print_translated_categories():
    df_json = pd.read_json('json_translate/clothes_translate.json')
    for index, row in df_json.iterrows():
        category_single_list.append(row['category'])
    category_single_list_1 = list(set(category_single_list))
    category_dict = {}
    for item in category_single_list_1:
        category_dict[item] = 1
    # pprint(category_single_list_1)
    with open('json_translate/categories_list_translated.json', 'w', encoding='utf-8') as f:
        json.dump(category_dict, f, ensure_ascii=False, indent=4)
    print('done')


def print_keys_dicts():
    with open('json_translate/layers_translated.json', 'r', encoding='utf-8') as f:
        layers_dict = json.load(f)

    with open('json_translate/categories_list_translated.json', 'r', encoding='utf-8') as f:
        categories_dict = json.load(f)

    # pprint(type(layers_dict.keys()))
    # value = {k: categories_dict[k] for k in set(categories_dict) - set(layers_dict)}
    for index, layer_key in enumerate(layers_dict.keys()):
        for category_key in categories_dict.keys():
            if layer_key == category_key:
                print(index, layer_key)


def get_keypoints():
    general_keypoints_dict = {}
    with open('files_extra/clothes.json', 'r', encoding='utf-8') as f:
        clothes_dict = json.load(f)
        for index, item in enumerate(clothes_dict['data']):

            # if index > 10:
            #     break
            print(index)
            front_photo_path = item['front_photo_path']
            try:
                with open(f'dressing_room_clothes_images/{front_photo_path}',
                          'rb') as f:
                    response = requests.post('https://hyperspace.dev.texelapps.com/api/v1/posenet/keypoints',
                                             files={'file': f})
                    general_keypoints_dict[item['_id']] = json.loads(response.text)
            except:
                continue
        with open('files_extra/keypoints.json', 'w', encoding='utf-8') as keypoints_file:
            json.dump(general_keypoints_dict, keypoints_file, ensure_ascii=False, indent=4)
        print('done')

    # with open('dressing_room_clothes_images/image_men_футболки/photo/104779/dressing_room_8673_1@2x.png', 'rb') as f:
    #     response = requests.post('https://hyperspace.dev.texelapps.com/api/v1/posenet/keypoints',
    #                              files={'file': f})
    #     keypoints = response.text
    #     print(keypoints)


def get_models_keypoints():
    general_keypoints_dict = {}
    images_list_1 = ['try_on_image_female.png', 'try_on_image_male.png']
    for image in images_list_1:
        try:
            with open(f'files_extra/{image}',
                      'rb') as f:
                response = requests.post('https://hyperspace.dev.texelapps.com/api/v1/posenet/keypoints',
                                         files={'file': f})
                if image.find('_female') != -1:
                    general_keypoints_dict['female'] = json.loads(response.text)
                else:
                    general_keypoints_dict['male'] = json.loads(response.text)
        except:
            continue
    with open('files_extra/keypoints_models.json', 'w', encoding='utf-8') as keypoints_file:
        json.dump(general_keypoints_dict, keypoints_file, ensure_ascii=False, indent=4)
    print('done')


def fix_blocked_for_shirts_into_clothes():
    with open('files_extra/clothes.json', 'r', encoding='utf-8') as f:
        clothes_dict = json.load(f)
        for index, item in enumerate(clothes_dict['data']):
            if item['category'] == 'Shirt' or item['category'] == 'T-shirt':
                clothes_dict['data'][index]['is_blocked'] = False
        with open('files_extra/clothes.json', 'w', encoding='utf-8') as clothes_file:
            json.dump(clothes_dict, clothes_file, ensure_ascii=False, indent=4)
        print('done')


def fix_images_for_skirts_into_clothes():
    with open('mapping_extra/clothes_women_skirts_translated.json', 'r', encoding='utf-8') as f:
        clothes_dict = json.load(f)
        for index, item in enumerate(clothes_dict):
            clothes_dict[index]['image'] = f'all_images/{item["image"]}'
        with open('mapping_extra/clothes_women_skirts_translated.json', 'w', encoding='utf-8') as clothes_file:
            json.dump(clothes_dict, clothes_file, ensure_ascii=False, indent=4)
        print('done')


def clothes_change_is_blocked():
    categories_tops_list = [
        "Mike",
        "Body.",
        "Polo",
        "Shirt",
        "T-shirt",
        "Top",
        "Blouse",
        "Tunic"
    ]
    with open('clothes_change_is_blocked/clothes.json', 'r', encoding='utf-8') as f:
        clothes_dict = json.load(f)
        for index, item in enumerate(clothes_dict['data']):
            if clothes_dict['data'][index]['category'] in categories_tops_list:
                clothes_dict['data'][index]['is_blocked'] = False
        with open('clothes_change_is_blocked/clothes.json', 'w', encoding='utf-8') as clothes_file:
            json.dump(clothes_dict, clothes_file, ensure_ascii=False, indent=4)
        print('done')


def add_price_into_clothes():
    with open('files_extra/clothes.json', 'r', encoding='utf-8') as f:
        clothes_dict = json.load(f)
        with open('files_extra/catalog_categories.json', 'r', encoding='utf-8') as catalog_categories_file:
            catalog_categories_dict = json.load(catalog_categories_file)
            with open('files_extra/catalog_categories_price.json', 'r',
                      encoding='utf-8') as catalog_categories_price_file:
                catalog_categories_price_dict = json.load(catalog_categories_price_file)
                for index, item in enumerate(clothes_dict['data']):
                    top_category = catalog_categories_dict[item['category']]
                    random_price = random.choice(catalog_categories_price_dict[top_category])
                    clothes_dict['data'][index]['price'] = random_price
                    print(top_category, random_price)
                with open('files_extra/clothes_with_price.json', 'w', encoding='utf-8') as clothes_file:
                    json.dump(clothes_dict, clothes_file, ensure_ascii=False, indent=4)
                print('done')


def set_blocked_for_smart_mirror_list():
    df = pd.read_excel("files_extra/Delete_list_Mix_n_Match_for_Smart_mirror.xlsx")
    ids_for_delete_list = df["id"].to_list()
    print(ids_for_delete_list)
    with open('files_extra/clothes.json', 'r', encoding='utf-8') as f:
        clothes_dict = json.load(f)
        count = 0
        for index, item in enumerate(clothes_dict['data']):
            if item["product_num"] in ids_for_delete_list:
                count += 1
                print(count, item["product_num"])
                clothes_dict['data'][index]["is_blocked"] = True
        with open('files_extra/clothes_blocked_smart_mirror.json', 'w', encoding='utf-8') as clothes_file:
            json.dump(clothes_dict, clothes_file, ensure_ascii=False, indent=4)
        print('done')


def add_is_sale_is_clothes():
    with open('files_extra/clothes.json', 'r', encoding='utf-8') as f:
        clothes_dict = json.load(f)
        for index, item in enumerate(clothes_dict['data']):
            clothes_dict['data'][index]["is_sale"] = random.choice([True, False])
        with open('files_extra/clothes_add_is_sale.json', 'w', encoding='utf-8') as clothes_file:
            json.dump(clothes_dict, clothes_file, ensure_ascii=False, indent=4)
        print('done')


def fix_layers_keys():
    with open('layers_reformat/layersTopsTuckedIn.json', 'r', encoding='utf-8') as f:
        layers_dict = json.load(f)
        new_layers_dict = {}
        print(layers_dict)
        for index, item in enumerate(layers_dict):
            new_key = item.lower().replace(".", "").replace("a ", "").replace("the ", "").replace(" ", "-")
            print(new_key)
            new_layers_dict[new_key] = layers_dict[item]
        print(111111111)
        print(new_layers_dict)
        with open('layers_reformat/layersTopsTuckedIn_reformatted.json', 'w', encoding='utf-8') as reformatted_layers_file:
            json.dump(new_layers_dict, reformatted_layers_file, ensure_ascii=False, indent=4)
        print('done')


def fix_catalog_categories_keys():
    with open('layers_reformat/catalog_categories.json', 'r', encoding='utf-8') as f:
        catalog_categories_dict = json.load(f)
        new_catalog_categories_dict = {}
        for index, item in enumerate(catalog_categories_dict):
            new_key = item.lower().replace(".", "").replace("a ", "").replace("the ", "").replace(" ", "-")
            new_catalog_categories_dict[new_key] = catalog_categories_dict[item]
        with open('layers_reformat/catalog_categories_reformatted.json', 'w',
                  encoding='utf-8') as reformatted_catalog_file:
            json.dump(new_catalog_categories_dict, reformatted_catalog_file, ensure_ascii=False, indent=4)
        print('done')


if __name__ == '__main__':
    # fix_catalog_categories_keys()
    fix_layers_keys()
    # add_is_sale_is_clothes()
    # set_blocked_for_smart_mirror_list()
    # add_price_into_clothes()
    # clothes_change_is_blocked()
    # fix_images_for_skirts_into_clothes()
    # parsing_static_html()
    # fix_blocked_for_shirts_into_clothes()
    # get_models_keypoints()
    # print_keys_dicts()
    # print_translated_categories()
    # clothes_translate()
    # layers_translate()
    # test_translate()
    # avatars_translate()
    # fix_resources()
    # fix_json_all_clothes()
    # fix_empty_into_all_clothes()
    # parsing_static_html()
    # dressing_room_photos_mapping()
    # get_links_for_dressing_room_clothes()
    # get_all_clothes()
    # get_detail_product_info()
    # toggle_product()
    # autopilot_mouse()
    # fix_exel_to_json()
    # get_resources()
    # is_blocked_mapping()
